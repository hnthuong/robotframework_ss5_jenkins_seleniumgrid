*** Settings ***
Library     SeleniumLibrary


*** Variables ***
${BROWSER}              chrome
${URL}                  https://demoqa.com/
${book_card}            //div[6][@class='card mt-4 top-card']
${login_btn}            //*[@id="login"]
${username}             //*[@id="userName"]
${password}             //*[@id="password"]
${book_store_btn}       gotoStore
${USER}                 normalqa1
${PASS}                 Normal!HN!1
${profile_menu}         //span[text()="Profile"]
${book1}                //a[text()="Git Pocket Guide"]/following::span[@id="delete-record-undefined"][1]
${book2}
...                     //a[text()="Learning JavaScript Design Patterns"]/following::span[@id="delete-record-undefined"][1]
${book3}
...                     //a[text()="Designing Evolvable Web APIs with ASP.NET"]/following::span[@id="delete-record-undefined"][1]
${book4}                //a[text()="Speaking JavaScript"]/following::span[@id="delete-record-undefined"][1]
${book5}                //a[text()="You Don't Know JS"]/following::span[@id="delete-record-undefined"][1]
${book6}
...                     //a[text()="Programming JavaScript Applications"]/following::span[@id="delete-record-undefined"][1]
${OK_btn}               //button[@id="closeSmallModal-ok"]
${verify}               //div[text()="No rows found"]
${logout_btn}           //button[@id="submit"]
${HUB URL}              http://192.168.197.6:5522
${PLATFORM}             WINDOWS

*** Test Cases ***
Go to Profile then delete all books
    Open Browser    ${URL}    ${BROWSER}    platform=${PLATFORM}    remote_url=${HUB URL}
    Maximize Browser Window
    Sleep    3
    Go to Book Store Application page
    Open login form
    Enter user name
    Enter password
    Click Login button
    Go to Profile page
    Delete all books from collection
    Verify that the list has been completely deleted
    Logout
    Close Browser


*** Keywords ***
Go to Book Store Application page
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    Click Element    ${book_card}
    Sleep    3

Open login form
    Click Element    ${login_btn}

Enter user name
    Input Text    ${username}    normalqa1

Enter password
    Input Text    ${password}    Normal!HN!1

Click Login button
    Click Element    ${login_btn}
    Sleep    3

Go to Profile page
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    Click Element    ${profile_menu}
    Sleep    2

Delete all books from collection
    Click Element    ${book1}
    Click Element    ${OK_btn}
    Handle Alert    ACCEPT

    Click Element    ${book2}
    Click Element    ${OK_btn}
    Handle Alert    ACCEPT

    Click Element    ${book3}
    Click Element    ${OK_btn}
    Handle Alert    ACCEPT

    Click Element    ${book4}
    Click Element    ${OK_btn}
    Handle Alert    ACCEPT

    Click Element    ${book5}
    Click Element    ${OK_btn}
    Handle Alert    ACCEPT

    Click Element    ${book6}
    Click Element    ${OK_btn}
    Handle Alert    ACCEPT

Logout
    Click Element    ${logout_btn}

Verify that the list has been completely deleted
    Element Text Should Be    ${verify}    No rows found
